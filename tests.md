# Firefox

## Firefox ESR

```bash
sudo snap install firefox --channel=esr/stable
```
If firefox is alreday installed
```bash
sudo snap refresh firefox --channel=esr/stable
```

Then we can create a policy by creating the file: /etc/firefox/policies/policies.json

## Change landing page

https://unix.stackexchange.com/questions/93795/how-to-set-firefox-homepage-from-terminal

With snap, path is slightly different and is placed in ~/snap/firefox/common

Can be accomplished with policies: https://mozilla.github.io/policy-templates/
**Need Firefox ESR!**

## Manipulate bookmarks

https://jeannicolasboulay.medium.com/bash-daily-get-firefox-bookmarks-with-the-terminal-7c86f0dc489

sqlite3 -line ~/snap/firefox/common/.mozilla/firefox/PROFILE_NAME.default/places.sqlite 'select moz_places.url, moz_bookmarks.title from moz_bookmarks join moz_places on moz_bookmarks.fk = moz_places.id'

`moz_bookmarks` fields
```
0|id|INTEGER|0||1
1|type|INTEGER|0||0
2|fk|INTEGER|0|NULL|0
3|parent|INTEGER|0||0
4|position|INTEGER|0||0
5|title|LONGVARCHAR|0||0
6|keyword_id|INTEGER|0||0
7|folder_type|TEXT|0||0
8|dateAdded|INTEGER|0||0
9|lastModified|INTEGER|0||0
10|guid|TEXT|0||0
11|syncStatus|INTEGER|1|0|0
12|syncChangeCounter|INTEGER|1|1|0
```
type: 1 for link, 2 for link folder

`moz_places` fields
```
0|id|INTEGER|0||1
1|url|LONGVARCHAR|0||0
2|title|LONGVARCHAR|0||0
3|rev_host|LONGVARCHAR|0||0
4|visit_count|INTEGER|0|0|0
5|hidden|INTEGER|1|0|0
6|typed|INTEGER|1|0|0
7|frecency|INTEGER|1|-1|0
8|last_visit_date|INTEGER|0||0
9|guid|TEXT|0||0
10|foreign_count|INTEGER|1|0|0
11|url_hash|INTEGER|1|0|0
12|description|TEXT|0||0
13|preview_image_url|TEXT|0||0
14|site_name|TEXT|0||0
15|origin_id|INTEGER|0||0
16|recalc_frecency|INTEGER|1|0|0
17|alt_frecency|INTEGER|0||0
18|recalc_alt_frecency|INTEGER|1|0|0
```

https://stackoverflow.com/questions/51124179/how-can-i-add-or-remove-a-bookmark-tag-in-firefox-via-the-command-line

```sql
INSERT INTO moz_bookmarks (type, parent, title, dateAdded)  
     VALUES (1,5,'MyBookmark',CURRENT_TIMESTAMP)
```

## Customize Firefox distribution

https://mozilla.github.io/policy-templates/
https://askubuntu.com/questions/1420240/how-to-add-edit-policies-json-of-firefox-snap
**Need Firefox ESR!**

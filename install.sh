#!/bin/sh

ANSIBLE_EX=$(which ansible)

if [ -z $ANSIBLE_EX ]; then
    echo "Ansible not found. Installing..."
    sudo apt install ansible
fi

ansible-galaxy collection install community.general
ansible-galaxy collection install community.crypto
ansible-galaxy collection install ansible.posix
ansible-galaxy install nickjj.docker

# General options

* `-i INVENTORYFILE`: indicates inventory file containing all hosts info
* `-i HOST1,[HOST2, ... ]: indicates hosts
* `--limit HOSTS`: limits command to specified hosts
* `-k`: ask for remote password
* `-K`: ask for remote sudo password
* `-u USER`: set remote user
* `--connection=local`: use local connector instead of ssh (if host is `localhost`)

# Commands

## Ping

ansible HOSTS -m ping -i INVENTORYFILE [--limit HOSTS]

`--limit` will only run command for selected `HOSTS`

ansible all -m ping -i HOST1, -u USER -k [-K]

## Run playbook

ansible-playbook PLAYBOOK


# Galaxy

Ansible `community.general`, `community.crypto`, `ansible.posix` and `nickjj.docker` can be installed:

```bash
./install.sh
```
